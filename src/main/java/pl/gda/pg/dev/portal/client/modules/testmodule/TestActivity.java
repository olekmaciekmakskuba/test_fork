package pl.gda.pg.dev.portal.client.modules.testmodule;

import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import pl.gda.pg.dev.portal.client.content.ContentActivity;
import pl.gda.pg.dev.portal.client.modules.testmodule.view.TestView;

@Singleton
public class TestActivity extends ContentActivity {

	private TestView view;

	@Inject
	public TestActivity(TestView view) {
		this.view = view;
	}

	@Override
	public Widget getView() {
		return view.asWidget();
	}
}
