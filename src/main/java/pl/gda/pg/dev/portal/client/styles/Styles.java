package pl.gda.pg.dev.portal.client.styles;

public class Styles {
	private Styles() {
	}

	private static Styles instance;

	public static Styles getInstance() {
		if (instance == null) {
			instance = new Styles();
		}
		return instance;
	}

	public String contentPanel() {
		return "content-panel";
	}

	public String contentWrapper() {
		return "content-wrapper";
	}

	public String headerPanel() {
		return "header-panel";
	}

	public String headerWrapper() {
		return "header-wrapper";
	}
}
