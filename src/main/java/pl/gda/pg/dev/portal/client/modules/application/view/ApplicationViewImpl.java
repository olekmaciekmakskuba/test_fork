package pl.gda.pg.dev.portal.client.modules.application.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class ApplicationViewImpl extends Composite implements ApplicationView {

	private static ApplicationViewUiBinder uiBinder = GWT.create(ApplicationViewUiBinder.class);

	@UiTemplate("ApplicationView.ui.xml")
	interface ApplicationViewUiBinder extends UiBinder<Widget, ApplicationViewImpl> {
	}

	@UiField
	SimplePanel headerPanel;
	@UiField
	SimpleLayoutPanel centerPanel;

	public ApplicationViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Inject
	private void init() {
		headerPanel.add(new InlineHTML("Hello World!"));
	}

	@Override
	public AcceptsOneWidget getContent() {
		return centerPanel;
	}

}
