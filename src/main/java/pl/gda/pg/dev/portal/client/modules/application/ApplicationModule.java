package pl.gda.pg.dev.portal.client.modules.application;

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import pl.gda.pg.dev.portal.client.content.ContentActivityMapper;
import pl.gda.pg.dev.portal.client.content.ContentPlaceHistoryMapper;
import pl.gda.pg.dev.portal.client.modules.application.view.ApplicationView;
import pl.gda.pg.dev.portal.client.modules.testmodule.TestPlace;

@Singleton
public class ApplicationModule {

	private ApplicationView view;

	@Inject
	public ApplicationModule(ApplicationView view) {
		this.view = view;
	}

	@Inject
	private void initContent(ContentActivityMapper contentActivityMapper, EventBus eventBus, ContentPlaceHistoryMapper historyMapper,
			PlaceController placeController, TestPlace testPlace) {
		ActivityManager activityManager = new ActivityManager(contentActivityMapper, eventBus);
		activityManager.setDisplay(view.getContent());
		PlaceHistoryHandler placeHistoryHandler = new PlaceHistoryHandler(historyMapper);
		placeHistoryHandler.register(placeController, eventBus, testPlace);
		placeHistoryHandler.handleCurrentHistory();
	}

	public Widget getView() {
		return view.asWidget();
	}
}
