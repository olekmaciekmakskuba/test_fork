package pl.gda.pg.dev.portal.client.modules.editor.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import pl.gda.pg.dev.portal.client.modules.codemirror.view.CodemirrorView;

@Singleton
public class EditorViewImpl extends Composite implements EditorView {

	private static EditorUiBinder uiBinder = GWT.create(EditorUiBinder.class);

	@UiTemplate("EditorView.ui.xml")
	interface EditorUiBinder extends UiBinder<Widget, EditorViewImpl> {
	}

	@UiField
	FlowPanel codemirror;
	@UiField
	FlowPanel switcher;

	public EditorViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Inject
	private void init(CodemirrorView codemirrorView) {
		codemirror.add(codemirrorView);
	}

	@Override
	public void setSwitcher(Widget view) {
		switcher.clear();
		switcher.add(view);
	}
}
