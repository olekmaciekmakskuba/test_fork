package pl.gda.pg.dev.portal.client.modules.editor.files.events.change;

import com.google.inject.Inject;
import pl.gda.pg.dev.portal.client.modules.codemirror.CodemirrorModule;
import pl.gda.pg.dev.portal.client.modules.codemirror.js.CodemirrorHistory;
import pl.gda.pg.dev.portal.client.modules.editor.files.EditorFile;
import pl.gda.pg.dev.portal.client.modules.editor.files.FilesController;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.SwitcherButtonsController;

public class FileChangeHandlerImpl implements FileChangeHandler {

	@Inject
	private FilesController filesController;
	@Inject
	private CodemirrorModule codemirrorModule;
	@Inject
	private SwitcherButtonsController buttonsController;

	@Override
	public void onFileChange(FileChangeEvent event) {
		saveCurrentState();

		String fileName = event.getFileName();
		buttonsController.setButtonSelected(fileName);
		loadFileState(fileName);

		codemirrorModule.setFocus();
	}

	private void loadFileState(String fileName) {
		EditorFile editorFile = filesController.getFileByNameAndSetAsCurrent(fileName);
		codemirrorModule.setText(editorFile.getContent());
		codemirrorModule.setHistory(editorFile.getHistory());
	}

	private void saveCurrentState() {
		String oldState = codemirrorModule.getText();
		CodemirrorHistory history = codemirrorModule.getHistory();
		filesController.saveContent(oldState, history);
	}
}
