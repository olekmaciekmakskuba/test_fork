package pl.gda.pg.dev.portal.client.modules.editor.popup;

import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import pl.gda.pg.dev.portal.client.modules.editor.files.FilesController;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.create.FileCreateEvent;
import pl.gda.pg.dev.portal.client.modules.editor.popup.view.PopupView;

public class PopupModule implements PopupView.Presenter {

	private final PopupView view;
	private final FilesController filesController;
	private EventBus eventBus;

	@Inject
	private PopupModule(PopupView view, FilesController filesController, EventBus eventBus) {
		this.view = view;
		this.filesController = filesController;
		this.eventBus = eventBus;
		view.setPresenter(this);
	}

	@Override
	public void submit(String fileName) {
		if (filesController.canCreateFile(fileName)) {
			eventBus.fireEvent(new FileCreateEvent(fileName));
		}
	}

	public void show() {
		view.show();
	}
}
