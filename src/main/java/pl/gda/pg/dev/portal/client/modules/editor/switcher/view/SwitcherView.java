package pl.gda.pg.dev.portal.client.modules.editor.switcher.view;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

public interface SwitcherView extends IsWidget {
	void addAddButtonHandler(ClickHandler clickHandler);

	void addPageButton(Widget pageButtonView);
}
