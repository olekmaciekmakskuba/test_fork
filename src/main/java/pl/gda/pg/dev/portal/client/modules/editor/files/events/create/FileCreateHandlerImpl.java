package pl.gda.pg.dev.portal.client.modules.editor.files.events.create;

import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import pl.gda.pg.dev.portal.client.modules.codemirror.CodemirrorModule;
import pl.gda.pg.dev.portal.client.modules.codemirror.js.CodemirrorHistory;
import pl.gda.pg.dev.portal.client.modules.editor.files.EditorFile;
import pl.gda.pg.dev.portal.client.modules.editor.files.FilesController;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.change.FileChangeEvent;

public class FileCreateHandlerImpl implements FileCreateHandler {

	@Inject
	private FilesController filesController;
	@Inject
	private CodemirrorModule codemirrorModule;
	@Inject
	private EventBus eventBus;

	@Override
	public void onCreateFile(FileCreateEvent event) {
		saveState();
		codemirrorModule.clearHistory();

		String fileName = event.getFileName();
		createNewFile(fileName);
		eventBus.fireEvent(new FileChangeEvent(fileName));
	}

	private void createNewFile(String fileName) {
		CodemirrorHistory emptyHistory = codemirrorModule.getHistory();
		EditorFile file = new EditorFile();
		file.setHistory(emptyHistory);
		file.setFileName(fileName);
		file.setContent("");
		filesController.addFile(file);
	}

	private void saveState() {
		final String text = codemirrorModule.getText();
		final CodemirrorHistory history = codemirrorModule.getHistory();
		filesController.saveContent(text, history);
	}
}
