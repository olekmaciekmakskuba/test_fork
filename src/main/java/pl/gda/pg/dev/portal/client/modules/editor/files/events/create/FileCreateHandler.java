package pl.gda.pg.dev.portal.client.modules.editor.files.events.create;

import com.google.gwt.event.shared.EventHandler;

public interface FileCreateHandler extends EventHandler {
	void onCreateFile(FileCreateEvent event);
}
