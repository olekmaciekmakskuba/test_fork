package pl.gda.pg.dev.portal.client.modules.editor.switcher;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.change.FileChangeEvent;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.presenter.SwitcherButtonPresenter;

@Singleton
public class SwitcherButtonFactory {

	@Inject
	private Provider<SwitcherButtonPresenter> provider;
	@Inject
	private EventBus eventBus;

	public SwitcherButtonPresenter get(String name) {
		SwitcherButtonPresenter presenter = provider.get();
		presenter.addClickHandler(createClickHandler(name));
		presenter.setFileName(name);
		return presenter;
	}

	private ClickHandler createClickHandler(final String name) {
		return new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new FileChangeEvent(name));
			}
		};
	}
}
