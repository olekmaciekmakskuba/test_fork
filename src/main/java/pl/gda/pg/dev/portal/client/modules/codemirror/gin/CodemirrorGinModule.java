package pl.gda.pg.dev.portal.client.modules.codemirror.gin;

import com.google.gwt.inject.client.AbstractGinModule;
import pl.gda.pg.dev.portal.client.modules.codemirror.view.CodemirrorView;
import pl.gda.pg.dev.portal.client.modules.codemirror.view.CodemirrorViewImpl;

public class CodemirrorGinModule extends AbstractGinModule {

	@Override
	protected void configure() {
		bind(CodemirrorView.class).to(CodemirrorViewImpl.class);
	}
}
