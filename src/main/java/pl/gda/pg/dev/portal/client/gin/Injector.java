package pl.gda.pg.dev.portal.client.gin;

import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;
import pl.gda.pg.dev.portal.client.modules.application.ApplicationModule;
import pl.gda.pg.dev.portal.client.scripts.ScriptsLoader;

@GinModules({ InstallerGinModule.class })
public interface Injector extends Ginjector {
	ApplicationModule getApplicationModule();

	ScriptsLoader getScriptsLoader();
}
