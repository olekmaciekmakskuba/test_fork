package pl.gda.pg.dev.portal.client.modules.testmodule.gin;

import com.google.gwt.inject.client.AbstractGinModule;
import pl.gda.pg.dev.portal.client.modules.testmodule.view.TestView;
import pl.gda.pg.dev.portal.client.modules.testmodule.view.TestViewImpl;

public class TestGinModule extends AbstractGinModule {
	@Override
	protected void configure() {
		bind(TestView.class).to(TestViewImpl.class);
	}
}
