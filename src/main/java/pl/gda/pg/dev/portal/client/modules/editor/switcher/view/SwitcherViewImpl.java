package pl.gda.pg.dev.portal.client.modules.editor.switcher.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;

@Singleton
public class SwitcherViewImpl extends Composite implements SwitcherView {

	private static SwitcherUiBinder uiBinder = GWT.create(SwitcherUiBinder.class);

	@UiTemplate("SwitcherView.ui.xml")
	interface SwitcherUiBinder extends UiBinder<Widget, SwitcherViewImpl> {
	}

	@UiField
	Button addButton;
	@UiField
	FlowPanel buttons;

	public SwitcherViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void addAddButtonHandler(ClickHandler clickHandler) {
		addButton.addClickHandler(clickHandler);
	}

	@Override
	public void addPageButton(Widget pageButtonView) {
		buttons.add(pageButtonView);
	}
}
