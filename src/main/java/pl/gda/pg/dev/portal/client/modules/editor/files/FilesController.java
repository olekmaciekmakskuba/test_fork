package pl.gda.pg.dev.portal.client.modules.editor.files;

import com.google.common.base.Optional;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import pl.gda.pg.dev.portal.client.modules.codemirror.js.CodemirrorHistory;
import pl.gda.pg.dev.portal.client.modules.editor.files.utils.EditorFileUtils;

import java.util.List;

@Singleton
public class FilesController {

	private Optional<EditorFile> currentFile = Optional.absent();
	private List<EditorFile> files = Lists.newArrayList();

	@Inject
	private EditorFileUtils utils;

	public void saveContent(String content, CodemirrorHistory history) {
		if (currentFile.isPresent()) {
			currentFile.get().setContent(content);
			currentFile.get().setHistory(history);
		}
	}

	public EditorFile getFileByNameAndSetAsCurrent(String name) {
		EditorFile file = Collections2.filter(files, utils.createFileByNameFilter(name)).iterator().next();
		currentFile = Optional.of(file);
		return file;
	}

	public boolean canCreateFile(String fileName) {
		return !Lists.transform(files, utils.getFileNameTransformer()).contains(fileName);
	}

	public void addFile(EditorFile file) {
		files.add(file);
	}
}
