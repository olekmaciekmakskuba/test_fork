package pl.gda.pg.dev.portal.client.modules.editor.switcher;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.inject.Singleton;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.presenter.SwitcherButtonPresenter;

import java.util.List;

@Singleton
public class SwitcherButtonsController {

	private List<SwitcherButtonPresenter> buttons = Lists.newArrayList();

	public void addButton(SwitcherButtonPresenter presenter) {
		buttons.add(presenter);
	}

	public void setButtonSelected(String name) {
		deactivateAll();
		Collections2.filter(buttons, createButtonByNameFilter(name)).iterator().next().activate();
	}

	private Predicate<SwitcherButtonPresenter> createButtonByNameFilter(final String name) {
		return new Predicate<SwitcherButtonPresenter>() {
			@Override
			public boolean apply(SwitcherButtonPresenter input) {
				return input.getFileName().equals(name);
			}
		};
	}

	private void deactivateAll() {
		for (SwitcherButtonPresenter presenter : buttons) {
			presenter.deactivate();
		}
	}
}
