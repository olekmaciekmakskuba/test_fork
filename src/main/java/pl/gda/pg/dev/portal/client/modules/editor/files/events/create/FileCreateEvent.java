package pl.gda.pg.dev.portal.client.modules.editor.files.events.create;

import com.google.gwt.event.shared.GwtEvent;

public class FileCreateEvent extends GwtEvent<FileCreateHandler> {

	public final static Type<FileCreateHandler> TYPE = new Type<>();

	private String fileName;

	public FileCreateEvent(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public Type<FileCreateHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(FileCreateHandler handler) {
		handler.onCreateFile(this);
	}

	public String getFileName() {
		return fileName;
	}
}
