package pl.gda.pg.dev.portal.client.styles;

public class EditorStyles {

	private EditorStyles() {
	}

	private static EditorStyles instance;

	public static EditorStyles getInstance() {
		if (instance == null) {
			instance = new EditorStyles();
		}
		return instance;
	}

	public String editorPanel() {
		return "editor-panel";
	}

	public String switcherAddButton() {
		return "switcher-add-button";
	}

	public String switcherButtonsWrapper() {
		return "switcher-buttons-wrapper";
	}

	public String switcherButton() {
		return "switcher-button";
	}

	public String switcherButtonActive() {
		return "switcher-button-active";
	}

	public String dialogCancelButton() {
		return "dialog-cancel-button";
	}

	public String dialogOkButton() {
		return "dialog-ok-button";
	}

	public String dialogText() {
		return "dialog-text";
	}

	public String dialogLabel() {
		return "dialog-label";
	}

	public String dialogBox() {
		return "dialog-box";
	}
}
