package pl.gda.pg.dev.portal.client.modules.editor.files;

import pl.gda.pg.dev.portal.client.modules.codemirror.js.CodemirrorHistory;

public class EditorFile {

	private CodemirrorHistory history;
	private String content;
	private String fileName;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public CodemirrorHistory getHistory() {
		return history;
	}

	public void setHistory(CodemirrorHistory history) {
		this.history = history;
	}

}
