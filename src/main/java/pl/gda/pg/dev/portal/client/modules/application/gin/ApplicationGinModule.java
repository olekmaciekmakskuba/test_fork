package pl.gda.pg.dev.portal.client.modules.application.gin;

import com.google.gwt.inject.client.AbstractGinModule;
import pl.gda.pg.dev.portal.client.modules.application.view.ApplicationView;
import pl.gda.pg.dev.portal.client.modules.application.view.ApplicationViewImpl;

public class ApplicationGinModule extends AbstractGinModule {
	@Override
	protected void configure() {
		bind(ApplicationView.class).to(ApplicationViewImpl.class);
	}
}
