package pl.gda.pg.dev.portal.client.modules.editor.files.utils;

import com.google.common.base.Predicate;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import pl.gda.pg.dev.portal.client.modules.editor.files.EditorFile;

@Singleton
public class EditorFileUtils {

	@Inject
	private FileNameTransformer fileNameTransformer;

	public FileNameTransformer getFileNameTransformer() {
		return fileNameTransformer;
	}

	public Predicate<EditorFile> createFileByNameFilter(final String name) {
		return new Predicate<EditorFile>() {
			@Override
			public boolean apply(EditorFile input) {
				return input.getFileName().equals(name);
			}
		};
	}
}
