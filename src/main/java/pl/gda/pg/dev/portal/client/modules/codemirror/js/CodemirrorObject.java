package pl.gda.pg.dev.portal.client.modules.codemirror.js;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Element;

public class CodemirrorObject extends JavaScriptObject {

	protected CodemirrorObject() {
	}

	public static CodemirrorObject initCodemirror(Element parent) {
		return initCodemirrorNative(parent);
	}

	public final void setContent(String content) {
		setContentNative(this, content);
	}

	public final String getContent() {
		return getContentNative(this);
	}

	public final void setFocus() {
		setFocusNative(this);
	}

	public final CodemirrorHistory getHistory() {
		return getHistoryNative(this);
	}

	public final void setHistory(CodemirrorHistory history) {
		setHistoryNative(this, history);
	}

	public final void clearHistory() {
		clearHistoryNative(this);
	}

	private native void clearHistoryNative(CodemirrorObject codemirror) /*-{
        codemirror.clearHistory();
    }-*/;

	private native void setHistoryNative(CodemirrorObject codemirror, CodemirrorHistory history) /*-{
        codemirror.setHistory(history);
    }-*/;

	private native CodemirrorHistory getHistoryNative(CodemirrorObject codemirror) /*-{
        return codemirror.getHistory();
    }-*/;

	private native void setFocusNative(CodemirrorObject codemirror) /*-{
        codemirror.focus();
    }-*/;

	private native String getContentNative(CodemirrorObject codemirror)/*-{
        return codemirror.getValue();
    }-*/;

	private native void setContentNative(CodemirrorObject codemirror, String content)/*-{
        codemirror.setValue(content);
    }-*/;

	private static native CodemirrorObject initCodemirrorNative(Element parent)/*-{
        return $wnd.CodeMirror(parent, {
            mode: "text/x-java",
            showCursorWhenSelecting: true,
            lineNumber: true,
            matchBrackets: true
        });
    }-*/;
}
