package pl.gda.pg.dev.portal.client.modules.editor.files.events.change;

import com.google.gwt.event.shared.GwtEvent;

public class FileChangeEvent extends GwtEvent<FileChangeHandler> {

	public final static Type<FileChangeHandler> TYPE = new Type<>();

	private String fileName;

	public FileChangeEvent(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public Type<FileChangeHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(FileChangeHandler handler) {
		handler.onFileChange(this);
	}

	public String getFileName() {
		return fileName;
	}
}
