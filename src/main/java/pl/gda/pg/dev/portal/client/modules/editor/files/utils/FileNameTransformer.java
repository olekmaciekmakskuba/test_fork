package pl.gda.pg.dev.portal.client.modules.editor.files.utils;

import com.google.common.base.Function;
import com.google.inject.Singleton;
import pl.gda.pg.dev.portal.client.modules.editor.files.EditorFile;

@Singleton
public class FileNameTransformer implements Function<EditorFile, String> {

	@Override
	public String apply(EditorFile input) {
		return input.getFileName();
	}
}
