package pl.gda.pg.dev.portal.client;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.gda.pg.dev.portal.client.gin.Injector;
import pl.gda.pg.dev.portal.client.scripts.ScriptsLoader;

public class ApplicationEntryPoint implements EntryPoint {

	public static final Injector INJECTOR = GWT.create(Injector.class);

	@Override
	public void onModuleLoad() {
		ScriptsLoader scriptsLoader = INJECTOR.getScriptsLoader();
		scriptsLoader.injectScripts(callback);
	}

	private Callback<Void, Exception> callback = new Callback<Void, Exception>() {
		@Override
		public void onFailure(Exception reason) {

		}

		@Override
		public void onSuccess(Void result) {
			createView();
		}
	};

	private void createView() {
		Widget applicationView = INJECTOR.getApplicationModule().getView();
		RootPanel.get().add(applicationView);
	}
}
