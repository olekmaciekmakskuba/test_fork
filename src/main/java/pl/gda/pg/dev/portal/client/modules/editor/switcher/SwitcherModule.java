package pl.gda.pg.dev.portal.client.modules.editor.switcher;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.create.FileCreateEvent;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.create.FileCreateHandler;
import pl.gda.pg.dev.portal.client.modules.editor.popup.PopupModule;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.presenter.SwitcherButtonPresenter;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.presenter.SwitcherPresenter;

@Singleton
public class SwitcherModule implements FileCreateHandler {

	private final SwitcherPresenter presenter;
	private final SwitcherButtonFactory switcherButtonFactory;
	private final Provider<PopupModule> popupModule;
	private final SwitcherButtonsController controller;

	@Inject
	public SwitcherModule(SwitcherPresenter presenter, SwitcherButtonFactory switcherButtonFactory, Provider<PopupModule> popupModule,
			SwitcherButtonsController controller, EventBus eventBus) {
		this.presenter = presenter;
		this.switcherButtonFactory = switcherButtonFactory;
		this.popupModule = popupModule;
		this.controller = controller;
		presenter.addAddButtonClickHandler(createClickHandler());
		eventBus.addHandler(FileCreateEvent.TYPE, this);
	}

	public Widget getView() {
		return presenter.getView();
	}

	private ClickHandler createClickHandler() {
		return new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				popupModule.get().show();
			}
		};
	}

	@Override
	public void onCreateFile(FileCreateEvent event) {
		SwitcherButtonPresenter switcherButtonPresenter = switcherButtonFactory.get(event.getFileName());
		presenter.addButton(switcherButtonPresenter);
		controller.addButton(switcherButtonPresenter);
	}
}
