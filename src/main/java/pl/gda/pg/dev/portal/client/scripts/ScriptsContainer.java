package pl.gda.pg.dev.portal.client.scripts;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.inject.Singleton;

import java.util.Arrays;
import java.util.Collection;

@Singleton
public class ScriptsContainer {

	private Function<Scripts, String> transformer = new Function<Scripts, String>() {
		@Override
		public String apply(Scripts scripts) {
			return scripts.getUrl();
		}
	};

	public Collection<String> getScriptsCollection() {
		return Collections2.transform(Arrays.asList(Scripts.values()), transformer);
	}
}
