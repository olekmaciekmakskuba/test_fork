package pl.gda.pg.dev.portal.client.scripts;

import com.google.gwt.core.client.Callback;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.Stack;

@Singleton
public class ScriptsLoader {

	private ScriptInjectorWrapper scriptInjector;
	private ScriptsContainer scriptsContainer;
	private Stack<String> stack;

	@Inject
	public ScriptsLoader(ScriptInjectorWrapper scriptInjector, ScriptsContainer scriptsContainer) {
		this.scriptInjector = scriptInjector;
		this.scriptsContainer = scriptsContainer;
	}

	public void injectScripts(Callback<Void, Exception> endOfScriptsAction) {
		stack = new Stack<>();
		stack.addAll(scriptsContainer.getScriptsCollection());
		injectScript(createNextScriptCallback(endOfScriptsAction));
	}

	private Callback<Void, Exception> createNextScriptCallback(final Callback<Void, Exception> endOfScriptsAction) {
		return new Callback<Void, Exception>() {

			@Override
			public void onFailure(Exception reason) {
			}

			@Override
			public void onSuccess(Void result) {
				if (stack.isEmpty()) {
					endOfScriptsAction.onSuccess(null);
				} else {
					injectScript(this);
				}
			}
		};
	}

	private void injectScript(Callback<Void, Exception> callback) {
		final String script = stack.pop();
		scriptInjector.fromUrl(script, callback);
	}
}
