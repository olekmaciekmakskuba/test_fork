package pl.gda.pg.dev.portal.client.scripts;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.ScriptInjector;
import com.google.inject.Singleton;

@Singleton
public class ScriptInjectorWrapper {

	public void fromUrl(String url, Callback<Void, Exception> callback) {
		ScriptInjector.fromUrl(url).setWindow(ScriptInjector.TOP_WINDOW).setCallback(callback).inject();
	}
}
