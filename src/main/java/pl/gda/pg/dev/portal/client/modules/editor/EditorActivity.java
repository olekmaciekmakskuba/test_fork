package pl.gda.pg.dev.portal.client.modules.editor;

import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import pl.gda.pg.dev.portal.client.content.ContentActivity;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.change.FileChangeEvent;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.change.FileChangeHandler;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.create.FileCreateEvent;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.create.FileCreateHandler;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.SwitcherModule;
import pl.gda.pg.dev.portal.client.modules.editor.view.EditorView;

@Singleton
public class EditorActivity extends ContentActivity {

	private EditorView view;

	@Inject
	public EditorActivity(EditorView editorView, EventBus eventBus, FileChangeHandler pageChangeHandler, FileCreateHandler fileCreateHandler,
			SwitcherModule switcher) {
		this.view = editorView;
		view.setSwitcher(switcher.getView());
		eventBus.addHandler(FileChangeEvent.TYPE, pageChangeHandler);
		eventBus.addHandler(FileCreateEvent.TYPE, fileCreateHandler);
	}

	@Override
	public Widget getView() {
		return view.asWidget();
	}
}
