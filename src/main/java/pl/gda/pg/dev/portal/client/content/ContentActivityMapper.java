package pl.gda.pg.dev.portal.client.content;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import pl.gda.pg.dev.portal.client.modules.editor.EditorActivity;
import pl.gda.pg.dev.portal.client.modules.editor.EditorPlace;
import pl.gda.pg.dev.portal.client.modules.testmodule.TestActivity;
import pl.gda.pg.dev.portal.client.modules.testmodule.TestPlace;

@Singleton
public class ContentActivityMapper implements ActivityMapper {

	@Inject
	private TestActivity testActivity;
	@Inject
	private EditorActivity editorActivity;

	@Override
	public Activity getActivity(Place place) {
		if (place instanceof TestPlace) {
			return testActivity;
		} else if (place instanceof EditorPlace) {
			return editorActivity;
		}
		return testActivity;
	}
}
