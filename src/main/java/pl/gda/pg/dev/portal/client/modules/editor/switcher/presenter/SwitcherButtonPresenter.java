package pl.gda.pg.dev.portal.client.modules.editor.switcher.presenter;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.view.button.SwitcherButtonView;

public class SwitcherButtonPresenter {

	@Inject
	private SwitcherButtonView view;
	private String name;

	public void setFileName(String fileName) {
		name = fileName;
		view.setFileName(fileName);
	}

	public void addClickHandler(ClickHandler clickHandler) {
		view.addClickHandler(clickHandler);
	}

	public void activate() {
		view.activate();
	}

	public void deactivate() {
		view.deactivate();
	}

	public Widget getView() {
		return view.asWidget();
	}

	public String getFileName() {
		return name;
	}
}
