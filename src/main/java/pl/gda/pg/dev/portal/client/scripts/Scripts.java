package pl.gda.pg.dev.portal.client.scripts;

public enum Scripts {
	CODEMIRROR("codemirror/codemirror.min.js");

	private String url;

	private Scripts(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}
}
