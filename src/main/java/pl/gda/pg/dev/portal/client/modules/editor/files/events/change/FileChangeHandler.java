package pl.gda.pg.dev.portal.client.modules.editor.files.events.change;

import com.google.gwt.event.shared.EventHandler;

public interface FileChangeHandler extends EventHandler {
	void onFileChange(FileChangeEvent event);
}
