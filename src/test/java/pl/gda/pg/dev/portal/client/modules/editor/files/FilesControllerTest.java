package pl.gda.pg.dev.portal.client.modules.editor.files;

import com.google.common.base.Predicate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.gda.pg.dev.portal.client.modules.codemirror.js.CodemirrorHistory;
import pl.gda.pg.dev.portal.client.modules.editor.files.utils.EditorFileUtils;
import pl.gda.pg.dev.portal.client.modules.editor.files.utils.FileNameTransformer;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FilesControllerTest {

	@InjectMocks
	private FilesController testObj;
	@Mock
	private FileNameTransformer fileNameTransformer;
	@Mock
	private EditorFileUtils utils;
	@Mock
	private Predicate<EditorFile> fileExistsPredicate;

	@Before
	public void init() {
		when(utils.getFileNameTransformer()).thenReturn(fileNameTransformer);
		when(utils.createFileByNameFilter(anyString())).thenReturn(fileExistsPredicate);
	}

	@Test
	public void shouldReturnFalse_whenFileAlreadyExists() {
		// given
		EditorFile file = new EditorFile();
		file.setFileName("button");
		testObj.addFile(file);
		when(fileNameTransformer.apply(file)).thenReturn("button");

		// when
		boolean result = testObj.canCreateFile("button");

		// then
		assertFalse(result);
	}

	@Test
	public void shouldReturnTrue_whenFileDoesNotExist() {
		// given
		EditorFile file = new EditorFile();
		file.setFileName("button");
		testObj.addFile(file);
		when(fileNameTransformer.apply(file)).thenReturn("button");

		// when
		boolean result = testObj.canCreateFile("Other_button");

		// then
		assertTrue(result);
	}

	@Test
	public void shouldSaveCurrentFileState() {
		// given
		CodemirrorHistory history = mock(CodemirrorHistory.class);
		EditorFile file = mock(EditorFile.class);
		when(file.getFileName()).thenReturn("button");
		testObj.addFile(file);
		when(fileExistsPredicate.apply(file)).thenReturn(true);

		// when
		EditorFile resultFile = testObj.getFileByNameAndSetAsCurrent("button");
		testObj.saveContent("content", history);

		// then
		assertEquals(file, resultFile);
		verify(file).setContent("content");
		verify(file).setHistory(history);
	}
}