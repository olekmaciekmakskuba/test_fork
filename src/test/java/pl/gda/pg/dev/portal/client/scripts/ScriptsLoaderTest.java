package pl.gda.pg.dev.portal.client.scripts;

import com.google.gwt.core.client.Callback;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ScriptsLoaderTest {

	@InjectMocks
	private ScriptsLoader testObj;
	@Mock
	private ScriptInjectorWrapper scriptInjectorWrapper;
	@Mock
	private ScriptsContainer scriptsContainer;
	@Captor
	private ArgumentCaptor<Callback<Void, Exception>> callbackCaptor;

	private List<String> scripts = new ArrayList<>();

	private Callback<Void, Exception> finalCallbackMock = mock(Callback.class);

	@Test
	public void shouldLoadOneScript() {
		// given
		scripts.add("script1");
		when(scriptsContainer.getScriptsCollection()).thenReturn(scripts);

		// when
		testObj.injectScripts(finalCallbackMock);
		verify(scriptInjectorWrapper).fromUrl(eq(scripts.get(0)), callbackCaptor.capture());
		callbackCaptor.getValue().onSuccess(null);

		// then
		verify(finalCallbackMock).onSuccess(null);
	}

	@Test
	public void shouldLoadNotCallFinalCallback_whenScriptsAreLoading() {
		// given
		scripts.add("script1");
		scripts.add("script2");
		when(scriptsContainer.getScriptsCollection()).thenReturn(scripts);

		// when
		testObj.injectScripts(finalCallbackMock);
		verify(scriptInjectorWrapper).fromUrl(eq(scripts.get(1)), callbackCaptor.capture());
		callbackCaptor.getValue().onSuccess(null);

		// then
		verify(finalCallbackMock, never()).onSuccess(null);
	}

	@Test
	public void shouldNotCallFinalCallback_whenLoadingFailed() {
		// given
		scripts.add("script1");
		when(scriptsContainer.getScriptsCollection()).thenReturn(scripts);

		// when
		testObj.injectScripts(finalCallbackMock);
		verify(scriptInjectorWrapper).fromUrl(eq(scripts.get(0)), callbackCaptor.capture());
		callbackCaptor.getValue().onFailure(null);

		// then
		verifyZeroInteractions(finalCallbackMock);
	}
}