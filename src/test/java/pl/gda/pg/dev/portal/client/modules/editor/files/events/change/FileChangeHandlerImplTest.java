package pl.gda.pg.dev.portal.client.modules.editor.files.events.change;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.gda.pg.dev.portal.client.modules.codemirror.CodemirrorModule;
import pl.gda.pg.dev.portal.client.modules.codemirror.js.CodemirrorHistory;
import pl.gda.pg.dev.portal.client.modules.editor.files.EditorFile;
import pl.gda.pg.dev.portal.client.modules.editor.files.FilesController;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.SwitcherButtonsController;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FileChangeHandlerImplTest {

	@InjectMocks
	private FileChangeHandlerImpl testObj;
	@Mock
	private FilesController filesController;
	@Mock
	private SwitcherButtonsController buttonsController;
	@Mock
	private CodemirrorModule codemirrorModule;
	@Mock
	private FileChangeEvent event;

	@Before
	public void init() {
		when(event.getFileName()).thenReturn("fileName");
	}

	@Test
	public void shouldSaveActualFile_andActivateNewOne() {
		// given
		CodemirrorHistory oldHistory = mock(CodemirrorHistory.class);
		when(codemirrorModule.getText()).thenReturn("text");
		when(codemirrorModule.getHistory()).thenReturn(oldHistory);

		CodemirrorHistory newHistory = mock(CodemirrorHistory.class);
		EditorFile file = new EditorFile();
		file.setFileName("fileName");
		file.setHistory(newHistory);
		file.setContent("new content");
		when(filesController.getFileByNameAndSetAsCurrent("fileName")).thenReturn(file);

		// when
		testObj.onFileChange(event);

		// then
		verify(filesController).saveContent("text", oldHistory);

		verify(buttonsController).setButtonSelected("fileName");

		verify(codemirrorModule).setText("new content");
		verify(codemirrorModule).setHistory(newHistory);

		verify(codemirrorModule).setFocus();
	}
}