package pl.gda.pg.dev.portal.client.modules.editor.files.utils;

import com.google.common.base.Predicate;
import org.junit.Test;
import pl.gda.pg.dev.portal.client.modules.editor.files.EditorFile;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EditorFileUtilsTest {

	private EditorFileUtils testObj = new EditorFileUtils();

	@Test
	public void shouldReturnTrue_whenNamesAreEquals() {
		// given
		EditorFile file = new EditorFile();
		file.setFileName("file");

		// when
		Predicate<EditorFile> predicate = testObj.createFileByNameFilter("file");
		boolean result = predicate.apply(file);

		// then
		assertTrue(result);
	}

	@Test
	public void shouldReturnFalse_whenNamesAreNotEquals() {
		// given
		EditorFile file = new EditorFile();
		file.setFileName("file");

		// when
		Predicate<EditorFile> predicate = testObj.createFileByNameFilter("other file");
		boolean result = predicate.apply(file);

		// then
		assertFalse(result);
	}
}