package pl.gda.pg.dev.portal.client.modules.editor.files.utils;

import org.junit.Test;
import pl.gda.pg.dev.portal.client.modules.editor.files.EditorFile;

import static org.junit.Assert.assertEquals;

public class FileNameTransformerTest {

	private FileNameTransformer testObj = new FileNameTransformer();

	@Test
	public void shouldReturnFileName() {
		// given
		EditorFile file = new EditorFile();
		file.setFileName("file");

		// when
		String result = testObj.apply(file);

		// then
		assertEquals("file", result);
	}
}