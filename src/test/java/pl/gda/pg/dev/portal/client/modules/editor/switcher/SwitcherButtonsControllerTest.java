package pl.gda.pg.dev.portal.client.modules.editor.switcher;

import org.junit.Test;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.presenter.SwitcherButtonPresenter;

import static org.mockito.Mockito.*;

public class SwitcherButtonsControllerTest {

	private SwitcherButtonsController testObj = new SwitcherButtonsController();

	@Test
	public void shouldSetSelectedOnlyOneButton() {
		// given
		SwitcherButtonPresenter firstPresenter = mock(SwitcherButtonPresenter.class);
		SwitcherButtonPresenter secondPresenter = mock(SwitcherButtonPresenter.class);
		when(firstPresenter.getFileName()).thenReturn("firstPresenter");
		when(secondPresenter.getFileName()).thenReturn("secondPresenter");
		testObj.addButton(firstPresenter);
		testObj.addButton(secondPresenter);

		// when
		testObj.setButtonSelected("firstPresenter");

		// then
		verify(firstPresenter).deactivate();
		verify(secondPresenter).deactivate();

		verify(firstPresenter).activate();
		verify(secondPresenter, never()).activate();
	}
}