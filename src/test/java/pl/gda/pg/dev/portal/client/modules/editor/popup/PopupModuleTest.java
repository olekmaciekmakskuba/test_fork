package pl.gda.pg.dev.portal.client.modules.editor.popup;

import com.google.web.bindery.event.shared.EventBus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.gda.pg.dev.portal.client.modules.editor.files.FilesController;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.create.FileCreateEvent;
import pl.gda.pg.dev.portal.client.modules.editor.popup.view.PopupView;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PopupModuleTest {

	@InjectMocks
	private PopupModule testObj;
	@Mock
	private PopupView view;
	@Mock
	private FilesController filesController;
	@Mock
	private EventBus eventBus;

	@Test
	public void shouldFireEvent_whenCanCreateNewFile() {
		// given
		when(filesController.canCreateFile("file")).thenReturn(true);

		// when
		testObj.submit("file");

		// then
		verify(eventBus).fireEvent(any(FileCreateEvent.class));
	}

	@Test
	public void shouldNotFireEvent_whenCanNotCreateNewFile() {
		// given
		when(filesController.canCreateFile("file")).thenReturn(true);

		// when
		testObj.submit("other file");

		// then
		verify(eventBus, never()).fireEvent(any(FileCreateEvent.class));
	}

	@Test
	public void shouldDelegateShowToView() {
		// when
		testObj.show();

		// then
		verify(view).show();
	}
}